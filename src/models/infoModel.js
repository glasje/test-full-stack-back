const { dbReign } = require('../config/database');
//dbReign.set('debug', true);
const Schema = dbReign.Schema;

const InfosSchema = new Schema({
    created_at: {
        type: String
    },
    title:{
        type: String
    },
    url:{
        type: String
    },
    author:{
        type: String
    },
    points: {
        type: Number
    },
    story_text: {
        type:String
    },
    comment_text: {
        type: String
    },
    num_comments: {
        type: Number
    },
    story_id:  {
        type: Number
    },
    story_title: {
        type: String
    },
    story_url: {
        type: String
    },
    parent_id: {
        type: Number
    },
    created_at_i:  {
        type: Number
    },
    _tags: {
        type: Array
    },
    objectID: {
        type: String
    },
    _highlightResult: {
        type:Object
    }

}, { versionKey: false });


module.exports = dbReign.model('infos', InfosSchema, 'infos');
