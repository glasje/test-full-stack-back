
const express = require("express");
const {
    getInfo,
    deleteInfo
} = require('./info.functions');
const app = express();



const getAllInfo = async (req, res)=> {
    try {
        let respuesta = await getInfo(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};
const removeInfo = async (req, res)=> {
    try {
        let respuesta = await deleteInfo(req)
        res.send(respuesta);
    } catch (e) {
        let respuesta = {
            error: true,
            msg: "Ha ocurrido un error!!",
            errorDetalle: e.toString()
        }
        res.send(respuesta);
    }
};


app.get("/api/reign/getInfo", getAllInfo);
app.get("/api/reign/removeInfo", removeInfo);



module.exports = app;