
const info = require('../../models/infoModel');
const apiHackerNews = require('../../api/servicios/info.services')
var Agenda = require('agenda');
var mongoConnectionString = process.env.urlMongo;
var agenda = new Agenda({db: {address: mongoConnectionString}});
const getInfo = async (req) => {

    let respuesta = await info.find({});
    
    if (respuesta !== null) {
        return {
            error: false,
            msg: "OK",
            data: respuesta
        };

    } else {
        return {
            error: false,
            msg: "No se logro obtener la información",
            data: null
        };
    }


}

const deleteInfo = async (req) => {
    let {id,create} = req.query;
    
    let respuesta =await info.deleteOne({objectID:id,created_at:create});
  
    if (respuesta !== null) {
        return {
            error: false,
            msg: "se elimino correctamente el registro",
            data: respuesta.deletedCount
        };

    } else {
        return {
            error: false,
            msg: "No se logro eliminar el registro",
            data: null
        };
    }


}
const setInfo = async (params) => {

    let respuesta = await info.create(params);
    return respuesta;
}

agenda.define('almacenarHackerNews',async function(job) {
    console.log("Nueva carga hacker news. Hora: " +
    new Date().getHours() + ":" + new Date().getMinutes());
    let consulta = await apiHackerNews.obtenerHackerNews();
    const {hits} =consulta;

    hits.map(async (hackerNews)=>{
        await setInfo(hackerNews);
        
    })
  });

  agenda.on('ready', function() {
    agenda.every('60 minutes', 'almacenarHackerNews');
    agenda.start(); 
  });

module.exports = {
    getInfo,
    deleteInfo
};
