const axios = require('axios');
require('../../config/config');

const obtenerHackerNews= async ()=>{

    const endPoint=`http://hn.algolia.com/`
    const method =`api/v1/search_by_date?query=nodejs`
    const url = `${endPoint}${method}`
 
    const resp = await axios.get(url,{
        headers: { 'Content-Type': 'application/json' }
    });

    if (resp.data.Results === 0) {
        throw new Error(`Ocurrio un error al obtener los Hacker News`);
    }
 
    
    return resp.data;
}


module.exports = { 
    obtenerHackerNews
}