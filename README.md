# test-full-stack-back
run `npm install` to install node_modules
verify port and environment in `.env` file must be PORT = 8080
ENVIRONMENT = prod to dockerize.
run `docker build -t test-full-stack-back .` to compile nodejs
run `docker-compose up` to dockerize nodejs with mongodb
check in the browser with `http://localhost:8080/api/reign/getInfo`

